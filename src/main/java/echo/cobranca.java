/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package echo;
import com.sun.media.sound.InvalidFormatException;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONObject;
import java.time.LocalDateTime;

/**
 *
 * @author itzme
 */
public class cobranca {
    float valor;
    String id;
    JSONObject objeto = new JSONObject();

    public cobranca(float valor, String id) {
        this.valor = valor;
        this.id = id;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "cobranca{" + "valor=" + valor + ", id=" + id + '}';
    }
    
    public void realizaCobranca (float valor, String id) throws IOException{
        
        try{
            objeto.put("id", "3fa85f64-5717-4562-b3fc-2c963f66afa6");
            objeto.put("status", "cobrança enviada");
            objeto.put("horaSolicitacao", LocalDateTime.now());
            objeto.put("horaFinalizacao", LocalDateTime.now());
            objeto.put("valor", "1");
            objeto.put("ciclista", "3fa85f64-5717-4562-b3fc-2c963f66afa6");
            FileWriter writeFile = new FileWriter ("dadosCobranca.json");
        }
        
        catch (IllegalArgumentException e422){
            objeto.put("id", "3fa85f64-5717-4562-b3fc-2c963f66afa6");
            objeto.put("código", "422");
            objeto.put("mensagem", "dados inválidos");
            FileWriter writeFile = new FileWriter ("dadosCobranca.json");
        }
    }
}
