/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package echo;

import com.sun.media.sound.InvalidFormatException;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONObject;

/**
 *
 * @author itzme
 */
public class enviarEmail {
    String id, email, msg, codigo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JSONObject getObjeto() {
        return objeto;
    }

    public void setObjeto(JSONObject objeto) {
        this.objeto = objeto;
    }
    
    JSONObject objeto = new JSONObject();
    
    
    public void retornaOK(String email, String mensagem) throws IOException{
        
    try{
    objeto.put("id", "3fa85f64-5717-4562-b3fc-2c963f66afa6");
    objeto.put("email", email);
    objeto.put("mensagem", "tudo ok");
    FileWriter writeFile = new FileWriter ("dadosEmail.json");
    }
    
    catch (NullPointerException e404){
        objeto.put("id", "3fa85f64-5717-4562-b3fc-2c963f66afa6");
        objeto.put("email", "nulo");
        objeto.put("mensagem", "email inexistente");
    }
    
    catch (IllegalArgumentException e422){ //todo validar email decentemente
        objeto.put("id", "3fa85f64-5717-4562-b3fc-2c963f66afa6");
        objeto.put("email", "nulo");
        objeto.put("mensagem", "email com formato inválido");
        FileWriter writeFile = new FileWriter ("dadosEmail.json");
    }
}
    
    
}
